import React from "react";
import "antd/dist/antd.css";
import { Input, Form, notification } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./Timeline.css";
import { addBooks } from "../../actions";
import CardBooks from "../cardsBooks/CardsBooks";
import CardBookAssessments from "./cardBookAssessments/CardBokAssessments";

const { Search } = Input;
class Timeline extends React.Component {
  state = {
    search: "",
    emptyInputError: false,
  };

  handleSearchState = (e) => {
    this.setState({ search: e.target.value });
  };

  resetInput = () => {
    this.setState({
      search: "",
    });
  };

  handleClick = () => {
    const { booksDispatch } = this.props;
    const inputSearch = this.state.search;
    if (
      inputSearch.length <= 0 ||
      inputSearch === "" ||
      inputSearch === undefined
    ) {
      return notification.error(
        {
          message: "Ops, erro na pesquisa",
          description: "Preencha o campo de search",
        },
        4
      );
    } else {
      this.setState({ ...this.state, emptyInputError: false });
      const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=${inputSearch}`;

      fetch(apiUrl)
        .then((res) => res.json())
        .then((res) => {
          booksDispatch(res.items);
          this.resetInput();
        });
    }
  };

  render() {
    const { books } = this.props;
    return (
      <div className="Container">
        <div className="StyleForm">
          <Form onFinish={this.handleClick}>
            <Search
              id="InputSearch"
              onSearch={this.handleClick}
              placeholder="Digite o nome de algum livro..."
              onChange={this.handleSearchState}
              enterButton
              compact
              size="large"
              style={{
                width: "60vw",
              }}
              value={this.state.search}
            />
          </Form>
        </div>
        <CardBooks booksMap={books} />
        <CardBookAssessments />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  books: state.books.books,
  state: state,
  wishlist: state.books.wishlist,
});

const mapDispatchToProps = (dispatch) => ({
  booksDispatch: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Timeline));
